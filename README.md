## What is this repository for? ##

This repo holds actually two examples

* first - stored on a **master** branch - is a trace record of development of a simple application that is able to calculate formulas like 2+3*4 etc.
its commits are thin grained so one can track the whole process of TDD cycle etc. (unfortunately, commits are written in polish, but maybe I will find some time to translate them)
* second - stored on a **bad_brackets** and **decent_brackets** branches - is an example of how vital to the projects maintenance and extensibility is the common understanding of used paradigms - in this case these are design patterns and coding practicies like single responsibility and low coupling.

## How to execute the application ##

To run the application all you need to do is to compile it (*mvn clean install*, as long as you have a maven installed on your machine)
and run it with pointing at class with main method (EquationCalculator). Something like *java -cp tdd-example-1.0-SNAPSHOT.jar com.abs.tract.tdd.presenter.view.EquationCalculator*
should do.

## Who is to blame? ##
if you have questions, threats or love letters to send, use this email: 
s2lomon@gmail.com.
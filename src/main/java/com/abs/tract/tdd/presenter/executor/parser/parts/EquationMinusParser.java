/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationBuilder;

/**
 *
 * @author Salomon
 */
public class EquationMinusParser extends FromBeginingSimpleValuePartsParserBase {

    private final EquationBuilder builder;

    public EquationMinusParser(EquationBuilder builder) {
        super("-");
        this.builder = builder;
    }

    @Override
    protected void handleValue(String searchedValue) {
        builder.addMinusSign();
    }
}

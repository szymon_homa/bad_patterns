/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationBuilder;

/**
 *
 * @author Salomon
 */
public class EquationPlusParser extends FromBeginingSimpleValuePartsParserBase {

    private final EquationBuilder equationBuilder;

    public EquationPlusParser(EquationBuilder equationBuilder) {
        super("\\+");
        this.equationBuilder = equationBuilder;
    }

    @Override
    protected void handleValue(String searchedValue) {
        equationBuilder.addPlusSign();
    }
}

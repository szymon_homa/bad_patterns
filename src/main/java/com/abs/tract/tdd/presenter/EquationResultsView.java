/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter;

/**
 *
 * @author Salomon
 */
public interface EquationResultsView {
    
    public void renderResults(boolean canBeRendered);
    
    public void updateLastResult(Double result);
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser;

/**
 *
 * @author Salomon
 */
public class EquationByPartsParser {

    private final EquationPartsParser parser;

    public EquationByPartsParser(EquationPartsParser parser) {
        this.parser = parser;
    }

    public void parse(String equation) {

        boolean parseSuccessful = true;
        while (anyCharToParse(equation) && parseSuccessful) {
            parser.parse(equation);
            equation = parser.getUnusedEquationPart();
            parseSuccessful = parser.isParseSuccessful();
        }
    }

    private boolean anyCharToParse(String equation) {
        return !equation.isEmpty();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter;

/**
 *
 * @author Salomon
 */
public interface EquationExecutor {

    public Double execute(String equation) throws IllegalArgumentException;
}

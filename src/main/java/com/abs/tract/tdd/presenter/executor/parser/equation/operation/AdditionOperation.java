/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation.operation;

/**
 *
 * @author Salomon
 */
public class AdditionOperation extends EquationOperationBase {

    private final Double argumentToAdd;

    public AdditionOperation(Double arg) {
        super(OperationPriority.Lower);
        this.argumentToAdd = arg;
    }

    @Override
    protected Double makeOperation(Double firstArg, Double secondArg) {
        return firstArg + secondArg;
    }

    @Override
    protected Double getArgument() {
        return argumentToAdd;
    }
}

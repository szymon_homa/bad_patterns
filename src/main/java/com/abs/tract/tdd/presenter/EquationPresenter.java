/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter;

/**
 *
 * @author Salomon
 */
public class EquationPresenter {

    private final EquationResultsView resultsView;
    private final EquationExecutor executor;
    private String equation;

    public EquationPresenter(EquationResultsView resultsView, EquationExecutor equationParser) {
        this.resultsView = resultsView;
        this.executor = equationParser;
    }

    public void setEquation(String equation) {
        this.equation = equation;
    }

    public void execute() {
        try {
            Double result = parseEquation();
            updateViewWithValidResult(result);
            allowRendering();
        } catch (IllegalArgumentException ex) {
            disallowRendering();
        }
    }

    private Double parseEquation() throws IllegalArgumentException {
        return executor.execute(equation);
    }

    private void updateViewWithValidResult(Double result) {
        resultsView.updateLastResult(result);
    }

    private void allowRendering() {
        resultsView.renderResults(true);
    }

    private void disallowRendering() {
        resultsView.renderResults(false);
    }
}

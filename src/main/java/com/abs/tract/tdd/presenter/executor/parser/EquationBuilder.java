/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser;

/**
 *
 * @author Salomon
 */
public interface EquationBuilder {

    public void addArgument(Double value);

    public void addPlusSign();

    public void addMinusSign();

    public void addMultiplicationSign();
}

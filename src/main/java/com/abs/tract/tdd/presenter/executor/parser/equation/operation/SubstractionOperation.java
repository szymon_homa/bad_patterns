/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation.operation;

/**
 *
 * @author Salomon
 */
public class SubstractionOperation extends EquationOperationBase {

    private final Double argument;

    public SubstractionOperation(Double argument) {
        super(OperationPriority.Lower);
        this.argument = argument;
    }

    @Override
    protected Double makeOperation(Double firstArg, Double secondArg) {
        return firstArg - secondArg;
    }

    @Override
    protected Double getArgument() {
        return argument;
    }
}

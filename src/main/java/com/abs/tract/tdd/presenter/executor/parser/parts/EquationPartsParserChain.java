/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;

/**
 *
 * @author Salomon
 */
public class EquationPartsParserChain implements EquationPartsParser {

    private final Iterable<EquationPartsParser> parsers;

    public EquationPartsParserChain(Iterable<EquationPartsParser> parsers) {
        this.parsers = parsers;

        throwExceptionWhenEmptyIteratorPassed(parsers);
    }

    @Override
    public void parse(String equation) throws IllegalArgumentException {
        for (EquationPartsParser parser : parsers) {
            parser.parse(equation);
            if (parser.isParseSuccessful()) {
                return;
            }
        }
    }

    @Override
    public boolean isParseSuccessful() {
        for (EquationPartsParser parser : parsers) {
            if (parser.isParseSuccessful()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String getUnusedEquationPart() {
        for (EquationPartsParser parser : parsers) {
            if (parser.isParseSuccessful()) {
                return parser.getUnusedEquationPart();
            }
        }
        return getFirstParserUnusedOutput();
    }

    private void throwExceptionWhenEmptyIteratorPassed(Iterable<EquationPartsParser> parsers) throws IllegalArgumentException {
        if (noneParserPassed(parsers)) {
            throw new IllegalArgumentException("Empty Parts Iterator Passed");
        }
    }

    private String getFirstParserUnusedOutput() {
        return parsers.iterator().next().getUnusedEquationPart();
    }

    private boolean noneParserPassed(Iterable<EquationPartsParser> parsers) {
        return !parsers.iterator().hasNext();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor;

import com.abs.tract.tdd.presenter.EquationExecutor;
import com.abs.tract.tdd.presenter.executor.parser.EquationByPartsParser;
import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;
import com.abs.tract.tdd.presenter.executor.parser.parts.EquationPlusParser;
import com.abs.tract.tdd.presenter.executor.parser.equation.Equation;
import com.abs.tract.tdd.presenter.executor.parser.parts.AllwaysExceptionThrowingParser;
import com.abs.tract.tdd.presenter.executor.parser.parts.EquationMinusParser;
import com.abs.tract.tdd.presenter.executor.parser.parts.EquationMultiplicationParser;
import com.abs.tract.tdd.presenter.executor.parser.parts.EquationPartsParserChain;
import com.abs.tract.tdd.presenter.executor.parser.parts.NumericValueParser;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Salomon
 */
public class CommonEquationExecutor implements EquationExecutor {

    @Override
    public Double execute(String equationSource) throws IllegalArgumentException {
        Equation equation = new Equation();
        EquationByPartsParser parser = new EquationByPartsParser(
                new EquationPartsParserChain(createPartsParsers(equation)));
        parser.parse(equationSource);
        return equation.execute();
    }

    private Iterable<EquationPartsParser> createPartsParsers(Equation equation) {
        List<EquationPartsParser> parsers = new ArrayList<EquationPartsParser>();
        parsers.add(new NumericValueParser(equation));
        parsers.add(new EquationMinusParser(equation));
        parsers.add(new EquationMultiplicationParser(equation));
        parsers.add(new EquationPlusParser(equation));
        parsers.add(new AllwaysExceptionThrowingParser());
        return parsers;
    }
}

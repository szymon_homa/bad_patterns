/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation.operation;

/**
 *
 * @author Salomon
 */
public class MultiplicationOperation extends EquationOperationBase {

    private final Double operationArgument;

    public MultiplicationOperation(Double operationArgument) {
        super(OperationPriority.Higher);
        this.operationArgument = operationArgument;
    }

    @Override
    protected Double makeOperation(Double firstArg, Double secondArg) {
        return firstArg * secondArg;
    }

    @Override
    protected Double getArgument() {
        return operationArgument;
    }
}

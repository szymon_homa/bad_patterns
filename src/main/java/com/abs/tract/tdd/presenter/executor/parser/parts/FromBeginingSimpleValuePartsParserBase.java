/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Salomon
 */
public abstract class FromBeginingSimpleValuePartsParserBase implements EquationPartsParser {

    private final Pattern pattern;
    private boolean isLastMatchSuccessful = false;
    private String lastUnusedEquationPart = "";

    public FromBeginingSimpleValuePartsParserBase(String searchedValueRegexp) {
        this.pattern = Pattern.compile("^(" + searchedValueRegexp + ")(.*)");
    }

    @Override
    public void parse(String equation) {

        Matcher matcher = createMatcher(equation);
        if (isMatchSuccessful(matcher)) {
            handleSearchedValue(matcher);
            updateUnusedEquationPart(matcher);
            updateMatchSuccessfulResult(true);
        } else {
            updateMatchSuccessfulResult(false);
            updateUnusedEquationPartWith(equation);
        }
    }

    @Override
    public boolean isParseSuccessful() {
        return isLastMatchSuccessful;
    }

    @Override
    public String getUnusedEquationPart() {
        return lastUnusedEquationPart;
    }

    protected abstract void handleValue(String searchedValue);

    private Matcher createMatcher(String equation) {
        Matcher matcher = pattern.matcher(equation);
        return matcher;
    }

    private boolean isMatchSuccessful(Matcher matcher) {
        return matcher.matches();
    }

    private void handleSearchedValue(Matcher matcher) {
        String searchedValue = getSearchedValue(matcher);
        handleValue(searchedValue);
    }

    private void updateUnusedEquationPart(Matcher matcher) {
        updateUnusedEquationPartWith(getUnusedPart(matcher));
    }

    private void updateUnusedEquationPartWith(String equation) {
        lastUnusedEquationPart = equation;
    }

    private void updateMatchSuccessfulResult(boolean result) {
        isLastMatchSuccessful = result;
    }

    private String getUnusedPart(Matcher matcher) {
        return matcher.group(2);
    }

    private String getSearchedValue(Matcher matcher) {
        String searchedValue = matcher.group(1);
        return searchedValue;
    }
}

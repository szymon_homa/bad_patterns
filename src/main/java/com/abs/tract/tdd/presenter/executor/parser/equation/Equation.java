/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation;

import com.abs.tract.tdd.presenter.executor.parser.EquationBuilder;
import com.abs.tract.tdd.presenter.executor.parser.equation.operation.SubstractionOperation;
import com.abs.tract.tdd.presenter.executor.parser.equation.operation.MultiplicationOperation;
import com.abs.tract.tdd.presenter.executor.parser.equation.operation.AdditionOperation;

/**
 *
 * @author Salomon
 */
public class Equation implements EquationBuilder {

    private final EquationOperationsExecutor operationExecutor = new EquationOperationsExecutor();
    private EquationOperationFactory currentFactory = EquationOperationFactory.AdditionFactory;

    public Double execute() {
        return operationExecutor.execute();
    }

    @Override
    public void addArgument(Double value) {
        EquationOperation operation = currentFactory.createOperation(value);
        operationExecutor.addOperation(operation);
    }

    @Override
    public void addPlusSign() {
        currentFactory = EquationOperationFactory.AdditionFactory;
    }

    @Override
    public void addMinusSign() {
        currentFactory = EquationOperationFactory.SubstractionFactory;
    }

    @Override
    public void addMultiplicationSign() {
        currentFactory = EquationOperationFactory.MultiplicationFactory;
    }

    private enum EquationOperationFactory {

        AdditionFactory {
            @Override
            public EquationOperation createOperation(Double arg) {
                return new AdditionOperation(arg);
            }
        },
        SubstractionFactory {
            @Override
            public EquationOperation createOperation(Double arg) {
                return new SubstractionOperation(arg);
            }
        },
        MultiplicationFactory {
            @Override
            public EquationOperation createOperation(Double arg) {
                return new MultiplicationOperation(arg);
            }
        };

        public abstract EquationOperation createOperation(Double arg);
    }
}

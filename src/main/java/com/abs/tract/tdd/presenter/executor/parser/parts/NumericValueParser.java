/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationBuilder;

/**
 *
 * @author Salomon
 */
public class NumericValueParser extends FromBeginingSimpleValuePartsParserBase {

    private final EquationBuilder builder;

    public NumericValueParser(EquationBuilder builder) {
        super("[\\d\\.]+");
        this.builder = builder;
    }

    @Override
    protected void handleValue(String searchedValue) {
        double parsed = Double.parseDouble(searchedValue);
        builder.addArgument(parsed);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;

/**
 *
 * @author Salomon
 */
public class AllwaysExceptionThrowingParser implements EquationPartsParser {

    @Override
    public void parse(String equation) throws IllegalArgumentException {
        throw new IllegalArgumentException("can't parse equation");
    }

    @Override
    public boolean isParseSuccessful() {
        return false;
    }

    @Override
    public String getUnusedEquationPart() {
        return "";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation;

/**
 *
 * @author Salomon
 */
public interface EquationOperation<T extends EquationOperation> {

    public Double executeOperationOn(Double argument);

    public boolean hasGreaterPriority(T operation);

    public void mergeWith(T operation);
}

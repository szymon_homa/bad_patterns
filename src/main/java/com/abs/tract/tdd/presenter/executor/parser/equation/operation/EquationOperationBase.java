/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation.operation;

import com.abs.tract.tdd.presenter.executor.parser.equation.EquationOperation;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Salomon
 */
public abstract class EquationOperationBase implements EquationOperation<EquationOperationBase> {

    private final OperationPriority priority;
    private final List<EquationOperationBase> mergedOperations = new ArrayList<EquationOperationBase>();

    public EquationOperationBase(OperationPriority priority) {
        this.priority = priority;
    }

    @Override
    public Double executeOperationOn(Double incomingArgument) {
        Double mergedResult = getArgument();
        for (EquationOperationBase operation : mergedOperations) {
            mergedResult = operation.executeOperationOn(mergedResult);
        }
        return makeOperation(incomingArgument, mergedResult);
    }

    @Override
    public boolean hasGreaterPriority(EquationOperationBase operation) {
        return compareOrdinals(operation);
    }

    @Override
    public void mergeWith(EquationOperationBase operation) {
        mergedOperations.add(operation);
    }

    protected abstract Double makeOperation(Double firstArg, Double secondArg);

    protected abstract Double getArgument();

    private boolean compareOrdinals(EquationOperationBase operation) {
        return priority.ordinal() > operation.priority.ordinal();
    }
}

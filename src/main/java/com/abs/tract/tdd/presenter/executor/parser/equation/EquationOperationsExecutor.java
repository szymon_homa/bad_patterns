/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Salomon
 */
public class EquationOperationsExecutor {

    private final List<EquationOperation> operations = new ArrayList<EquationOperation>();

    public Double execute() {
        Double result = 0.0;
        for (EquationOperation operation : operations) {
            result = operation.executeOperationOn(result);
        }
        return result;
    }

    public void addOperation(EquationOperation nextOperation) {
        if (hasGreaterPriorityThanLast(nextOperation)) {
            mergeWithLastOperation(nextOperation);
        } else {
            operations.add(nextOperation);
        }
    }

    private EquationOperation getLastOperation() {
        return operations.get(operations.size() - 1);
    }

    private void mergeWithLastOperation(EquationOperation nextOperation) {
        getLastOperation().mergeWith(nextOperation);
    }

    private boolean hasGreaterPriorityThanLast(EquationOperation nextOperation) {
        if (anyOperationExists()) {
            return nextOperation.hasGreaterPriority(getLastOperation());
        }
        return false;
    }

    private boolean anyOperationExists() {
        return !operations.isEmpty();
    }
}

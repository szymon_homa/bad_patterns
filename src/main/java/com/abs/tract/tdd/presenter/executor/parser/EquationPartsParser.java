/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser;

/**
 *
 * @author Salomon
 */
public interface EquationPartsParser {

    public void parse(String equation) throws IllegalArgumentException;

    public boolean isParseSuccessful();

    public String getUnusedEquationPart();
}

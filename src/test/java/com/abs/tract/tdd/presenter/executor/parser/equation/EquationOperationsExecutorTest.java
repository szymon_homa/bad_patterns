/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class EquationOperationsExecutorTest {

    private final EquationOperationsExecutor executor = new EquationOperationsExecutor();
    private final EquationOperation firstOperation = mock(EquationOperation.class);
    private final EquationOperation secondOperation = mock(EquationOperation.class);
    private final Double firstOperationResult = 5.0;
    private final Double secondOperationResult = 10.0;

    @Before
    public void setUp() {
        when(firstOperation.executeOperationOn(anyDouble())).thenReturn(firstOperationResult);
        when(firstOperation.hasGreaterPriority(anyOperation())).thenReturn(false);

        when(secondOperation.executeOperationOn(firstOperationResult)).thenReturn(secondOperationResult);
        when(secondOperation.hasGreaterPriority(anyOperation())).thenReturn(false);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testExecuteWithNoOperationsAdded() {
        Double expected = 0.0;
        Double result = executor.execute();

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testExecuteWithOneOperationAdded() {
        Double expected = firstOperationResult;

        when(firstOperation.executeOperationOn(0.0)).thenReturn(firstOperationResult);

        executor.addOperation(firstOperation);
        Double result = executor.execute();

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testExecuteWithTwoOperationsAdded() {
        Double expected = secondOperationResult;

        executor.addOperation(firstOperation);
        executor.addOperation(secondOperation);
        Double result = executor.execute();

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testAddOperationWithGreaterPriorityThanLastAdded() {
        when(secondOperation.hasGreaterPriority(firstOperation)).thenReturn(true);

        executor.addOperation(firstOperation);
        executor.addOperation(secondOperation);

        verify(firstOperation).mergeWith(secondOperation);
    }

    private EquationOperation anyOperation() {
        return any(EquationOperation.class);
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class EquationPresenterTest {

    private final EquationResultsView resultsView = mock(EquationResultsView.class);
    private final EquationExecutor equationParser = mock(EquationExecutor.class);
    private final EquationPresenter presenter = new EquationPresenter(resultsView, equationParser);

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testExecuteEquationWithValidParse() {
        String toParse = "equationToParse";
        Double expectedResult = 15.0;

        when(equationParser.execute(toParse)).thenReturn(expectedResult);

        presenter.setEquation(toParse);
        presenter.execute();

        verify(resultsView).renderResults(true);
        verify(resultsView).updateLastResult(expectedResult);
    }

    @Test
    public void testExecuteInvalidEquation() {
        String toParse = "equationToParse";

        when(equationParser.execute(toParse)).thenThrow(new IllegalArgumentException());

        presenter.setEquation(toParse);
        presenter.execute();

        verify(resultsView).renderResults(false);
        verifyNoMoreInteractions(resultsView);
    }

    @Test
    public void testSetEquationWithoutExecute() {
        presenter.setEquation("any Equation");

        verifyZeroInteractions(resultsView, equationParser);
    }

    @Test(expected = NullPointerException.class)
    public void testExecuteWhenParserThrowsUndeclaredException() {

        when(equationParser.execute(anyString())).thenThrow(new NullPointerException());

        presenter.setEquation("any Equation");
        presenter.execute();
    }
}
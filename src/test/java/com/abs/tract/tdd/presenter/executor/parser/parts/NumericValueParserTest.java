/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationBuilder;
import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class NumericValueParserTest extends EquationPartsParserTestBase {

    private final EquationBuilder builder = mock(EquationBuilder.class);
    private final NumericValueParser parser = new NumericValueParser(builder);
    private final String validOnlyEquation = "125.5";
    private final String invalidEquation = "invalidEquation";

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testParseOneDiggitOnTheBeggining() {
        String equation = "5";
        parser.parse(equation);

        verify(builder).addArgument(5.0);
    }

    @Test
    public void testParseTwoDiggitOnTheBeggining() {
        String equation = "50";
        parser.parse(equation);

        verify(builder).addArgument(50.0);
    }

    @Test
    public void testDoubleOnTheBeggining() {
        String equation = "125.5";
        parser.parse(equation);

        verify(builder).addArgument(125.5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseWithMallformedNumber() {
        String equation = "125.5.5";
        parser.parse(equation);
    }

    @Override
    protected EquationPartsParser getEquationPartsParser() {
        return parser;
    }

    @Override
    protected String getValidOnlyEquation() {
        return validOnlyEquation;
    }

    @Override
    protected String getInvalidEquation() {
        return invalidEquation;
    }
}
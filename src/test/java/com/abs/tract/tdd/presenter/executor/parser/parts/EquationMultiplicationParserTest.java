/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationBuilder;
import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class EquationMultiplicationParserTest extends EquationPartsParserTestBase {

    private final EquationBuilder builder = mock(EquationBuilder.class);
    private final EquationMultiplicationParser parser = new EquationMultiplicationParser(builder);
    private final String validEquation = "*";
    private final String invalidEquation = "invalidEquation";

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSingleMultiplication() {
        parser.parse(validEquation);

        verify(builder).addMultiplicationSign();
    }

    @Test
    public void testSingleDoubleMultiplication() {
        parser.parse(validEquation + validEquation);

        verify(builder).addMultiplicationSign();
        assertEquals(validEquation, parser.getUnusedEquationPart());
    }

    @Override
    protected EquationPartsParser getEquationPartsParser() {
        return parser;
    }

    @Override
    protected String getValidOnlyEquation() {
        return validEquation;
    }

    @Override
    protected String getInvalidEquation() {
        return invalidEquation;
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class CommonEquationExecutorTest {

    private final CommonEquationExecutor executor = new CommonEquationExecutor();

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testExecuteSimpleSum() {
        String equation = "2+2";
        Double result = executor.execute(equation);

        assertEquals(4.0, result, 0.001);
    }

    @Test
    public void testExecuteCoupleSums() {
        String equation = "2+2+2";
        Double result = executor.execute(equation);

        assertEquals(6.0, result, 0.001);
    }

    @Test
    public void testExecuteWithMinus() {
        String equation = "3-1";
        Double result = executor.execute(equation);

        assertEquals(2.0, result, 0.0001);
    }

    @Test
    public void testExecuteWithDoubleMinus() {
        String equation = "3-1-5";
        Double result = executor.execute(equation);

        assertEquals(-3.0, result, 0.000001);
    }

    @Test
    public void testExecuteWithCoupleMinusesAndPluses() {
        String equation = "2+3+4-4-3";
        Double result = executor.execute(equation);

        assertEquals(2.0, result, 0.000001);
    }

    @Test
    public void testExecuteWithSimpleMultiplication() {
        String equation = "2*3";
        Double result = executor.execute(equation);

        assertEquals(6.0, result, 0.000001);
    }

    @Test
    public void testExecuteWithSumAndSimpleMultiplication() {
        String equation = "2+2*3";
        Double result = executor.execute(equation);

        assertEquals(8.0, result, 0.000001);
    }

    @Test
    public void testExecuteWithSumAndCoupleMultiplications() {
        String equation = "2+2*3*5-4*5";
        Double result = executor.execute(equation);

        assertEquals(12.0, result, 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteWithUnsuportedParameter() {
        String unsuported = "unsuported";
        executor.execute(unsuported);
    }
}
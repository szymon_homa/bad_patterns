/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation.operation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.InOrder;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class EquationOperationBaseTest {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testExecuteOperationOn() {
        Double initialValue = 2.0;
        Double operationArgument = 5.0;
        TestCaseEquationOperationBase operation = new TestCaseEquationOperationBase(operationArgument);

        Double expected = operation.makeOperation(initialValue, operationArgument);
        Double result = operation.executeOperationOn(initialValue);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    public void testExecuteOperationOnWithOneOperationMerged() {
        Double initialValue = 2.0;
        Double operationArgument = 5.0;
        Double mergedOperationResult = 10.0;

        EquationOperationBase mergedOperation = mock(EquationOperationBase.class);
        when(mergedOperation.executeOperationOn(operationArgument)).thenReturn(mergedOperationResult);

        TestCaseEquationOperationBase operation = spy(new TestCaseEquationOperationBase(operationArgument));
        operation.mergeWith(mergedOperation);

        operation.executeOperationOn(initialValue);

        InOrder inOrder = inOrder(operation, mergedOperation);
        inOrder.verify(operation).getArgument();
        inOrder.verify(mergedOperation).executeOperationOn(operationArgument);
        inOrder.verify(operation).makeOperation(initialValue, mergedOperationResult);
    }
    
    @Test
    public void testExecuteOperationOnWithTwoOperationsMerged() {
        Double initialValue = 2.0;
        Double operationArgument = 5.0;
        Double firstMergedResult = 10.0;
        Double secondMergedResult = 15.0;

        EquationOperationBase firstMerged = mock(EquationOperationBase.class);
        EquationOperationBase secondMerged = mock(EquationOperationBase.class);
        when(firstMerged.executeOperationOn(operationArgument)).thenReturn(firstMergedResult);
        when(secondMerged.executeOperationOn(firstMergedResult)).thenReturn(secondMergedResult);

        TestCaseEquationOperationBase operation = spy(new TestCaseEquationOperationBase(operationArgument));
        operation.mergeWith(firstMerged);
        operation.mergeWith(secondMerged);

        operation.executeOperationOn(initialValue);

        InOrder inOrder = inOrder(operation, firstMerged, secondMerged);
        inOrder.verify(operation).getArgument();
        inOrder.verify(firstMerged).executeOperationOn(operationArgument);
        inOrder.verify(secondMerged).executeOperationOn(firstMergedResult);
        inOrder.verify(operation).makeOperation(initialValue, secondMergedResult);
    }

    @Test
    public void testHasGreaterPriority() {
        TestCaseEquationOperationBase operationWithHigherPriority = new TestCaseEquationOperationBase(OperationPriority.Higher);
        TestCaseEquationOperationBase operationWithLowerPriority = new TestCaseEquationOperationBase(OperationPriority.Lower);

        assertTrue(operationWithHigherPriority.hasGreaterPriority(operationWithLowerPriority));
        assertFalse(operationWithHigherPriority.hasGreaterPriority(operationWithHigherPriority));
        assertFalse(operationWithLowerPriority.hasGreaterPriority(operationWithHigherPriority));
    }

    private class TestCaseEquationOperationBase extends EquationOperationBase {

        private Double operationArgument;

        public TestCaseEquationOperationBase(OperationPriority priority) {
            super(priority);
        }

        public TestCaseEquationOperationBase(Double operationArgument) {
            super(OperationPriority.Higher);
            this.operationArgument = operationArgument;
        }

        @Override
        protected Double makeOperation(Double firstArg, Double secondArg) {
            return firstArg * secondArg;
        }

        @Override
        protected Double getArgument() {
            return operationArgument;
        }
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Salomon
 */
public class AllwaysExceptionThrowingParserTest {

    private final AllwaysExceptionThrowingParser parser = new AllwaysExceptionThrowingParser();

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParse() {
        parser.parse("any input");
    }

    @Test
    public void testIsParseSuccessful() {

        assertFalse(parser.isParseSuccessful());
    }

    @Test
    public void testGetUnussedEquationPart() {

        assertEquals("", parser.getUnusedEquationPart());
    }
}
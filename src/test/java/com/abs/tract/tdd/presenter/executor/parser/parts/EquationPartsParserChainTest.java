/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;
import java.util.Arrays;
import java.util.Collections;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class EquationPartsParserChainTest {

    private final EquationPartsParser firstParser = mock(EquationPartsParser.class);
    private final EquationPartsParser secondParser = mock(EquationPartsParser.class);
    private final String toParse = "toParse";
    private EquationPartsParserChain chain = new EquationPartsParserChain(Arrays.asList(firstParser, secondParser));

    @Before
    public void setUp() {
        when(firstParser.isParseSuccessful()).thenReturn(false);
        when(secondParser.isParseSuccessful()).thenReturn(false);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testParseWithOneUnsuccessfulyParsingParserInChain() {

        chain = new EquationPartsParserChain(Arrays.asList(firstParser));
        chain.parse(toParse);

        verify(firstParser).parse(toParse);
    }

    @Test
    public void testParseWithTwoUnsuccessfulyParsingParsersInChain() {

        chain.parse(toParse);

        verify(firstParser).parse(toParse);
        verify(secondParser).parse(toParse);
    }

    @Test
    public void testParseWithOneSuccessfulyAndOneUnsuccessfulyParsingParsersInChain() {

        when(firstParser.isParseSuccessful()).thenReturn(true);

        chain.parse(toParse);

        verify(firstParser).parse(toParse);
        verifyZeroInteractions(secondParser);
    }

    @Test
    public void testIsParseSuccessfulWhenNoParserParsedSuccessfuly() {

        assertFalse(chain.isParseSuccessful());
    }

    @Test
    public void testIsParseSuccessfulWhenOneParserParsedSuccessfuly() {
        when(secondParser.isParseSuccessful()).thenReturn(true);

        assertTrue(chain.isParseSuccessful());
    }

    @Test
    public void testGetUnusedEquationPartWhenNonParserParsedSuccessfuly() {
        String firstParserUnusedPart = "firstParserUnusedPart";
        String secondParserUnusedPart = "secondParserUnusedPart";
        when(firstParser.getUnusedEquationPart()).thenReturn(firstParserUnusedPart);
        when(secondParser.getUnusedEquationPart()).thenReturn(secondParserUnusedPart);

        assertEquals(firstParserUnusedPart, chain.getUnusedEquationPart());
    }

    @Test
    public void testGetUnusedEquationPartWhenSecondParserParsedSuccessfuly() {
        String firstParserUnusedPart = "firstParserUnusedPart";
        String secondParserUnusedPart = "secondParserUnusedPart";
        when(firstParser.getUnusedEquationPart()).thenReturn(firstParserUnusedPart);
        when(secondParser.getUnusedEquationPart()).thenReturn(secondParserUnusedPart);
        when(secondParser.isParseSuccessful()).thenReturn(true);

        assertEquals(secondParserUnusedPart, chain.getUnusedEquationPart());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateChainWithNoParts() {
        new EquationPartsParserChain(Collections.EMPTY_LIST);
    }
}
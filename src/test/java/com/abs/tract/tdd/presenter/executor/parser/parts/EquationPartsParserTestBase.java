/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Salomon
 */
@Ignore
public abstract class EquationPartsParserTestBase {

    @Test
    public void testIsParseSuccessfulWhenValidEquationPassed() {
        EquationPartsParser parser = getEquationPartsParser();
        parser.parse(getValidOnlyEquation());

        assertTrue(parser.isParseSuccessful());
    }

    @Test
    public void testIsParseSuccessfulWhenInvalidEquationPassed() {
        EquationPartsParser parser = getEquationPartsParser();
        parser.parse(getInvalidEquation());

        assertFalse(parser.isParseSuccessful());
    }

    @Test
    public void testIsParseSuccessfulWhenNoEquationParsed() {
        EquationPartsParser parser = getEquationPartsParser();

        assertFalse(parser.isParseSuccessful());
    }

    @Test
    public void testGetUnusedEquationPartWhenValidOnlyEquationPassed() {
        EquationPartsParser parser = getEquationPartsParser();

        parser.parse(getValidOnlyEquation());
        String unusedPart = parser.getUnusedEquationPart();

        assertTrue(unusedPart.isEmpty());
    }

    @Test
    public void testGetUnusedEquationPartWhenValidWithAdditionsEquationPassed() {
        EquationPartsParser parser = getEquationPartsParser();

        String expectedAdditions = "expectedAdditions";
        String equation = getValidOnlyEquation() + expectedAdditions;
        parser.parse(equation);
        String unusedPart = parser.getUnusedEquationPart();

        assertEquals(expectedAdditions, unusedPart);
    }

    @Test
    public void testGetUnusedEquationPartWhenInvalidEquationPassed() {
        EquationPartsParser parser = getEquationPartsParser();
        String expectedPart = getInvalidEquation();

        parser.parse(getInvalidEquation());
        String unusedPart = parser.getUnusedEquationPart();

        assertEquals(expectedPart, unusedPart);
    }

    @Test
    public void testGetUnusedEquationPartWhenNoEquationParsed() {
        EquationPartsParser parser = getEquationPartsParser();

        String unusedPart = parser.getUnusedEquationPart();

        assertTrue(unusedPart.isEmpty());
    }
//
//    @Test
//    public void testDoubleParseWithValidAndInvalidEquation() {
//        EquationPartsParser parser = getEquationPartsParser();
//
//        String
//
//        parser.parse(getValidOnlyEquation());
//        parser.parse(getInvalidEquation());
//
//        String unusedEquationPart = parser.getUnusedEquationPart();
//
//        assertFalse(parser.isParseSuccessful());
//        
//    }

    protected abstract EquationPartsParser getEquationPartsParser();

    protected abstract String getValidOnlyEquation();

    protected abstract String getInvalidEquation();
}

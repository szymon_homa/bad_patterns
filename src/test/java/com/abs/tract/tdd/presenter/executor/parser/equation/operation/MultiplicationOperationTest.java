/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation.operation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class MultiplicationOperationTest {

    private final Double operationArg = 2.0;
    private final MultiplicationOperation operation = new MultiplicationOperation(operationArg);

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMakeOperation() {
        Double firstArg = 2.0;
        Double secondArg = 4.0;
        Double expected = firstArg * secondArg;

        Double result = operation.makeOperation(firstArg, secondArg);

        assertEquals(expected, result);
    }

    @Test
    public void testGetArgument() {

        assertEquals(operationArg, operation.getArgument());
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 *
 * @author Salomon
 */
public class FromBeginingSimpleValuePartsParserBaseTest extends EquationPartsParserTestBase {

    private final String searchedValue = "searchedValue";
    private final ForTestFromBeginingSimpleValuePartsParser parser = spy(new ForTestFromBeginingSimpleValuePartsParser(searchedValue));
    private final String validEquation = searchedValue;
    private String invalidEquation = "invalidEquation";

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testParseOneSearchedValue() {
        parser.parse(searchedValue);

        verify(parser).handleValue(searchedValue);
    }

    @Test
    public void testParseDoubleSumSign() {
        parser.parse(searchedValue + searchedValue);

        verify(parser).handleValue(searchedValue);
        assertEquals(searchedValue, parser.getUnusedEquationPart());
    }

    @Override
    protected EquationPartsParser getEquationPartsParser() {
        return parser;
    }

    @Override
    protected String getValidOnlyEquation() {
        return validEquation;
    }

    @Override
    protected String getInvalidEquation() {
        return invalidEquation;
    }

    private class ForTestFromBeginingSimpleValuePartsParser extends FromBeginingSimpleValuePartsParserBase {

        public ForTestFromBeginingSimpleValuePartsParser(String searchedValueRegexp) {
            super(searchedValueRegexp);
        }

        @Override
        protected void handleValue(String searchedValue) {
        }
    }
}
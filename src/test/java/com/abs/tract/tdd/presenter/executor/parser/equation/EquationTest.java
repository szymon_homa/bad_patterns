/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class EquationTest {

    private final Equation equation = new Equation();

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testExecuteWithNoBuilderMethodsInvoked() {
        Double result = equation.execute();

        assertEquals(0.0, result, 0.0000000001);
    }

    @Test
    public void testExecuteAfterOneAddInvoked() {
        Double valueToAdd = 2.0;
        equation.addArgument(valueToAdd);
        Double result = equation.execute();

        assertEquals(valueToAdd, result, 0.0000000001);
    }

    @Test
    public void testExecuteAfterTwoArgsWithPlusInvoked() {
        Double firstValue = 2.0;
        Double secondValue = 3.0;
        Double expected = firstValue + secondValue;

        equation.addArgument(firstValue);
        equation.addPlusSign();
        equation.addArgument(secondValue);
        Double result = equation.execute();

        assertEquals(expected, result, 0.0000000001);
    }

    @Test
    public void testExecuteAfterThreArgsWithPlusInvoked() {
        Double firstValue = 2.0;
        Double secondValue = 3.0;
        Double thirdValue = 4.0;
        Double expected = firstValue + secondValue + thirdValue;

        equation.addArgument(firstValue);
        equation.addPlusSign();
        equation.addArgument(secondValue);
        equation.addPlusSign();
        equation.addArgument(thirdValue);
        Double result = equation.execute();

        assertEquals(expected, result, 0.0000000001);
    }

    @Test
    public void testExecuteAfterTwoArgsWithMinusInvoked() {
        Double firstValue = 4.0;
        Double secondValue = 3.0;
        Double expected = firstValue - secondValue;

        equation.addArgument(firstValue);
        equation.addMinusSign();
        equation.addArgument(secondValue);
        Double result = equation.execute();

        assertEquals(expected, result, 0.0000000001);
    }

    @Test
    public void testExecuteSimpleMultiplication() {
        Double firstValue = 4.0;
        Double secondValue = 3.0;
        Double expected = firstValue * secondValue;

        equation.addArgument(firstValue);
        equation.addMultiplicationSign();
        equation.addArgument(secondValue);
        Double result = equation.execute();

        assertEquals(expected, result, 0.0000000001);
    }

    @Test
    public void testExecuteSumAndMultiplication() {
        Double firstValue = 2.0;
        Double secondValue = 2.0;
        Double thirdValue = 3.0;
        Double expected = firstValue + secondValue * thirdValue;

        equation.addArgument(firstValue);
        equation.addPlusSign();
        equation.addArgument(secondValue);
        equation.addMultiplicationSign();
        equation.addArgument(thirdValue);

        Double result = equation.execute();

        assertEquals(expected, result, 0.0000000001);
    }
}
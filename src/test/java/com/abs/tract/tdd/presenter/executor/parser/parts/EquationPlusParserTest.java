/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationBuilder;
import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 *
 * @author Salomon
 */
public class EquationPlusParserTest extends EquationPartsParserTestBase {

    private final EquationBuilder builder = mock(EquationBuilder.class);
    private final EquationPlusParser parser = new EquationPlusParser(builder);
    private final String validEquation = "+";
    private String invalidEquation = "invalidEquation";

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testParseSimpleSumSign() {
        String plus = "+";
        parser.parse(plus);

        verify(builder).addPlusSign();
    }

    @Test
    public void testParseDoubleSumSign() {
        String doublePlus = "++";
        parser.parse(doublePlus);

        verify(builder).addPlusSign();
        assertEquals("+", parser.getUnusedEquationPart());
    }

    @Override
    protected EquationPartsParser getEquationPartsParser() {
        return parser;
    }

    @Override
    protected String getValidOnlyEquation() {
        return validEquation;
    }

    @Override
    protected String getInvalidEquation() {
        return invalidEquation;
    }
}
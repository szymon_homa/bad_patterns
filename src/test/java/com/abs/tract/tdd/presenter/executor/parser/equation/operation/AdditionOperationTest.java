/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.equation.operation;

import com.abs.tract.tdd.presenter.executor.parser.equation.EquationOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class AdditionOperationTest {

    private final Double firstArg = 2.0;
    private final Double secondArg = 3.0;
    private final AdditionOperation operation = new AdditionOperation(firstArg);

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testExecuteOperationOn() {
        Double expected = secondArg + firstArg;

        Double result = operation.makeOperation(firstArg, secondArg);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    public void testGetArgument() {

        assertEquals(firstArg, operation.getArgument());
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser.parts;

import com.abs.tract.tdd.presenter.executor.parser.EquationBuilder;
import com.abs.tract.tdd.presenter.executor.parser.EquationPartsParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class EquationMinusParserTest extends EquationPartsParserTestBase {

    private final EquationBuilder builder = mock(EquationBuilder.class);
    private final String validValue = "-";
    private final String invalidValue = "invalidValue";
    private final EquationMinusParser parser = new EquationMinusParser(builder);

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testParseWithSimpleValue() {
        parser.parse(validValue);

        verify(builder).addMinusSign();
    }

    @Test
    public void testParseWithDoubledValue() {
        parser.parse(validValue + validValue);

        verify(builder).addMinusSign();
        assertEquals(validValue, parser.getUnusedEquationPart());
    }

    @Override
    protected EquationPartsParser getEquationPartsParser() {
        return parser;
    }

    @Override
    protected String getValidOnlyEquation() {
        return validValue;
    }

    @Override
    protected String getInvalidEquation() {
        return invalidValue;
    }
}
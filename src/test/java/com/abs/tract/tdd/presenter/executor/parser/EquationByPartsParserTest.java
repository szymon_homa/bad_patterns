/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.tdd.presenter.executor.parser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import static org.mockito.Mockito.*;

/**
 *
 * @author Salomon
 */
public class EquationByPartsParserTest {

    private final EquationPartsParser partParser = mock(EquationPartsParser.class);

    @Before
    public void setUp() {
        when(partParser.getUnusedEquationPart()).thenReturn("");
        when(partParser.isParseSuccessful()).thenReturn(true);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testParseEquationWithOneSuccessfulyParsingPartAndNoUnsuedPart() {
        String equation = "equation";
        EquationByPartsParser parser = new EquationByPartsParser(partParser);

        parser.parse(equation);

        verify(partParser).parse(equation);
    }

    @Test(timeout = 100)
    public void testParseEquationWithOneNotSuccessfulyParsingPartAndHoleEquationAsUnussedRetriving() {
        String equation = "equation";
        EquationByPartsParser parser = new EquationByPartsParser(partParser);

        when(partParser.isParseSuccessful()).thenReturn(false);
        when(partParser.getUnusedEquationPart()).thenReturn(equation);

        parser.parse(equation);

        verify(partParser).parse(equation);
    }

    @Test
    public void testParseEquationWithOneSuccessfulyParsingPartAndSomeUnusedPart() {
        String equation = "equation";
        String unusedPart = "unusedPart";
        EquationByPartsParser parser = new EquationByPartsParser(partParser);

        when(partParser.isParseSuccessful()).thenReturn(true);
        when(partParser.getUnusedEquationPart()).thenReturn(unusedPart, "");

        parser.parse(equation);

        InOrder inOrder = inOrder(partParser);
        inOrder.verify(partParser).parse(equation);
        inOrder.verify(partParser).parse(unusedPart);
    }
}